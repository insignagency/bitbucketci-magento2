<?php
/*
  Ce fichier provient du repository bitbucketci-magento2
*/
namespace Deployer;
require __DIR__ . '/sets.php';

task('cachetool:clear:opcache', function () {
    run("if [ ! -f {{deploy_path}}/cachetool.phar ]; then cd {{deploy_path}}/; curl -sO https://gordalina.github.io/cachetool/downloads/cachetool-4.1.1.phar && mv cachetool-4.1.1.phar cachetool.phar && chmod +x cachetool.phar ; fi");
    run("cd {{release_path}} && php {{deploy_path}}/cachetool.phar opcache:reset --fcgi={{cachetool}}");
});

// For the first deploy, ensure necessary paths are present
desc('Setup');
task('deploy:setup', function() {
   run('if [ ! -L {{deploy_path}}/current ]; then ln -s {{deploy_path}}/tmp/ {{deploy_path}}/current; fi');
   run('if [ ! -d "{{deploy_path}}/shared/pub/" ]; then mkdir {{deploy_path}}/shared/pub/; fi');
   run('if [ ! -d "{{deploy_path}}/current/pub/" ]; then mkdir {{deploy_path}}/current/pub/; fi');
   run('if [ ! -d "{{deploy_path}}/shared/pub/static" ]; then mkdir {{deploy_path}}/shared/pub/static; fi');
   run('if [ ! -d "{{deploy_path}}/shared/pub/media" ]; then mkdir {{deploy_path}}/shared/pub/media; fi');
   run('if [ ! -L "{{deploy_path}}/current/pub/static" ]; then ln -s {{deploy_path}}/shared/pub/static {{deploy_path}}/current/pub/static; fi');
}
);

// IMPORTANT, while subfolder static is removed during deploy, parent has to be setgid to ensure that static will belong to group
desc('Writable Setgid');
task('deploy:writable:setgid', function() {
        run('chmod g+ws {{deploy_path}}/shared/pub/');
}
);

desc('Preserve artifact static dir');
task('deploy:static:preserve', function() {
        writeln(run('ls -al {{release_path}}/pub/static/'));
        run('mv {{release_path}}/pub/static {{release_path}}/pub/static_deploying');
        writeln(run('ls -al {{release_path}}/pub/static_deploying/'));
}
)->onRoles('web1');

// move old static and delete after 'deploy:symlink' ?
desc('Sync static shared dir with release');
task('deploy:static:activate', function() {
        run('rm -rf {{deploy_path}}/shared/pub/static');
        run('mv {{release_path}}/pub/static_deploying {{deploy_path}}/shared/pub/static');
})->onRoles('web1');

desc('Make and deploy artifact via rsync');
task('deploy:artifact', function() {
   runLocally('tar -zcvf /tmp/artifact.tgz -C ../../../ .');
   upload('/tmp/artifact.tgz','{{release_path}}/artifact.tgz');
   run('tar -zxvf {{release_path}}/artifact.tgz -C {{release_path}}/ --strip 1');
}
);

/*
* A utiliser lorsque le serveur cible n'a pas rsync
*/
desc('Make and deploy artifact via scp');
task('deploy:artifact:scp', function() {
   runLocally('tar -zcvf /tmp/artifact.tgz -C ../../../ .');
   runLocally('scp /tmp/artifact.tgz '. Context::get()->getHost() . ':{{release_path}}');
   run('tar -zxvf {{release_path}}/artifact.tgz -C {{release_path}}/ --strip 1');
}
);

desc('Backup database');
task('deploy:database:backup', function() {
    upload('../misc/deployer-mysql-dump.sh','{{deploy_path}}/deployer-mysql-dump.sh; cd {{deploy_path}}/; chmod +x deployer-mysql-dump.sh;');
    run('cd {{deploy_path}}/; ./deployer-mysql-dump.sh {{deploy_path}} {{deploy_path}}/current/');
}
)->onRoles('prod');

desc('Clean magento cache');
task('deploy:cache:flush', function() {
        run('cd {{release_path}}; bin/magento cache:flush');
}
);

desc('Clean magento cache');
task('magento:cache:flush', function() {
    run('cd {{release_path}}; /usr/bin/php bin/magento cache:flush');
}
);

desc('Clean redis cache');
task('magento:redis:flushall', function() {
    run('cd {{release_path}}; redis-cli FLUSHALL');
}
);

desc('Set production mode');
task('deploy:magento:setupmode', function() {
        run('cd {{release_path}}; bin/magento deploy:mode:set production --skip-compilation');
}
)->onRoles('web1');

desc('Set developer mode');
task('deploy:magento:setupmodedev', function() {
        run('cd {{release_path}}; bin/magento deploy:mode:set developer');
}
)->onStage('develop');

desc('Setup Upgrade');
task('deploy:magento:setupupgrade', function() {
        run('cd {{release_path}}; bin/magento setup:upgrade');
}
)->onRoles('web1');

desc('Setup Install');
task('deploy:magento:setup:install', function() {
        run('cd {{release_path}}; bin/magento setup:install');
}
)->onRoles('web1');

desc('Configs set');
task('deploy:magento:configset', function() {
    run('cd {{release_path}}; bin/magento config:set dev/js/enable_js_bundling 0');
    run('cd {{release_path}}; bin/magento config:set dev/js/minify_files 1');
    run('cd {{release_path}}; bin/magento config:set dev/js/merge_files 1');
    run('cd {{release_path}}; bin/magento config:set dev/css/minify_files 1');
    run('cd {{release_path}}; bin/magento config:set dev/css/merge_css_files 1');
}
)->onRoles('web1');

desc('Composer install');
task('composer:19:install', function () {
    run("if [ ! -f {{deploy_path}}/composer.phar ]; then cd {{deploy_path}}; curl https://getcomposer.org/installer | /usr/bin/php ; fi");
    run("/usr/bin/php {{deploy_path}}/composer.phar self-update 1.9.1");
    run("cd {{release_path}}/; /usr/bin/php {{deploy_path}}/composer.phar install --no-dev --no-progress --prefer-dist");
});

desc('Upgrade magento database');
task('deploy:magento:upgradedb', function () {
    run("cd {{release_path}}; /usr/bin/php bin/magento setup:db-schema:upgrade");
    run("cd {{release_path}}; /usr/bin/php bin/magento setup:db-data:upgrade");
});

desc('Clean redis cache');
task('magento:redis:flushall', function() {
    run('cd {{release_path}}; redis-cli FLUSHALL');
}
);

desc('Clean magento cache');
task('magento:cache:flush', function() {
        run('cd {{release_path}}; bin/magento cache:flush');
}
);

desc('Enable maintenance mode');
task('magento:maintenance:enable', function () {
    run("cd {{release_path}}; bin/magento maintenance:enable;");
});

desc('Disable maintenance mode');
task('magento:maintenance:disable', function () {
    run("cd {{release_path}}; bin/magento maintenance:disable;");
});

desc('Test');
task('magento:test', function () {
    echo $ent->getHostname();
});

desc('Generate Feed Google Merchant Center');
task('magento:generate:feed', function () {
    run("cd {{release_path}}; bin/magento feed:profile:generate 8;");
});

desc('TesMagento debug');
task('magento:debug', function () {
    $result = run("cd {{release_path}}; cat app/etc/env.php; cat app/etc/config.php");
    writeln("$result");
});