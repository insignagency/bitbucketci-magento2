<?php
/*
  Ce fichier provient du repository bitbucketci-magento2
*/
namespace Deployer;

set('default_timeout', 7200);
set('keep_releases', '4');
set('writable_use_sudo', false);
set('writable_mode', 'acl'); // chmod, chown, chgrp or acl.
set('writable_recursive', true);
// Configuration
set('shared_files', [
        'app/etc/env.php',
        'var/.maintenance.ip'
    ]
);
set('shared_dirs', [
        'pub/media',
        'pub/static',
        'var/logistic',
        'pub/newsletter-assets'
    ]
);
set('writable_dirs', [
        'generated',
        'var',
        'pub/static',
        'pub/media',
        'seo'
    ]
);

set('clear_paths', [
    'generated/*',
    'var/cache/*',
]);