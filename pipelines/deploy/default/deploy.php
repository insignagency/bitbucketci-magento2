<?php
/*
  Ce fichier provient du repository bitbucketci-magento2
*/
namespace Deployer;
require __DIR__ . '/tasks.php';

desc('Deploy release on web servers');
task(
    'deploy', [
        'deploy:info',
        'deploy:prepare',
        'deploy:release',
        'deploy:artifact',
        'deploy:writable',
        'deploy:static:preserve',
        'deploy:shared',
        'magento:debug',
        'deploy:magento:setup:install',
        'deploy:magento:configset',
        'deploy:magento:setupupgrade',
        'deploy:magento:setupmode',
        'magento:maintenance:enable',
        'deploy:static:activate',
        'deploy:shared',
        'magento:cache:flush',
        'cachetool:clear:opcache',
        'magento:maintenance:disable',
        'deploy:symlink',
        'cleanup',
        'success'
    ]
);

