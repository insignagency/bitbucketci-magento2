#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-magento2
#
if [[ ! "$0" =~ ^/ ]]; then abspath="$(pwd)/$0"; else abspath="$0"; fi
. pipelines/common.sh

apt-get update
apt install -y curl
export DOCKER_COMPOSE_VERSION=1.29.1
export DOCKER_COMPOSE_URL=https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)
curl -L $DOCKER_COMPOSE_URL > docker-compose
chmod +x docker-compose
mv docker-compose /usr/local/bin
# le plugin docker de bitbucket posant des problèmes sur le mapping de user on va devoir faire un chmod 777, 
# donc on ne travaille pas dans le répertoire source pour ne pas pourrir les droits des artifacts qui seront poussés en prod. 
#cp -R . /tmp/workspace-test
#mv /tmp/workspace-test ./
#cd workspace-test
# on modifie les chemins des volumes de type "~/xxx" ou "xxx" car bitbucket interdit le montage de volumes extérieurs ou de volumes internes
php /factory/transpose-docker-compose.php > docker-compose-ci.yml
# network non utilisé, juste là pour éviter erreur docker car ce network est utilisé dans le docker-compose.yml
docker network create traefik
docker-compose -f docker-compose-ci.yml up -d
docker-compose -f docker-compose-ci.yml ps
cd ./pipelines/deploy/misc/ && ./sync-ci-db.sh $SYNC_CI_DB_FROM && cd ../../../
cp app/etc/env-docker-compose.php app/etc/env.php
# un chown -R www-data:www-data ./ est inopérant car les montages docker dans la CI n'autorise pas le mapping de user   
chmod -R 777 var pub generated
cp ~/.composer/auth.json ./
docker-compose -f docker-compose-ci.yml exec -T -w /var/www/web php pipelines/build/default/composer-version.sh
docker-compose -f docker-compose-ci.yml exec -T -u www-data -w /var/www/web php bash -c "export test_site_domain=$test_site_domain; pipelines/build/default/install-cms.sh"
#find ./ -type d -not -path "./pipelines/*" -exec chmod 770 {} \;
#find ./ -type f -not -path "./pipelines/*" -exec chmod 660 {} \;

rm ./auth.json
track "Affichage du fichier docker-compose-ci.yml"
cat docker-compose-ci.yml
track  "Affichage du fichier app/etc/env.php"
cat app/etc/env.php
ls -al var/