#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-magento2
#
set -e
t=$(mysql -h 127.0.0.1 -u root -pdb_on_docker -e 'show databases;' 2>&1 > /dev/null); 
if [ "$?" -gt 0 ] ; then 
  t=$(mysql -h mysql -u root -pdb_on_docker -e 'show databases;' 2>&1 > /dev/null); 
  if [ "$?" -gt 0 ] ; then 
    echo "Mysql ne répond ni sur le host '127.0.0.1' ni sur le host 'mysql'";
    exit 1
  else
    mysql_host="mysql"
  fi
else
  mysql_host="127.0.0.1"
fi
set -x
mysql -h $mysql_host -u root -pdb_on_docker website -e "update core_config_data set value=\"http://$test_site_domain\" where path='web/unsecure/base_url' or path='web/secure/base_url';"
mysql -h $mysql_host -u root -pdb_on_docker website -e "UPDATE core_config_data SET value=0 WHERE path='admin/url/use_custom';"
mysql -h $mysql_host -u root -pdb_on_docker website -e "DELETE FROM core_config_data WHERE scope='stores' AND scope_id=0;"
composer install --no-dev --no-progress
echo "Enable all modules"
bin/magento module:enable --all
bin/magento setup:install 
bin/magento setup:upgrade
bin/magento config:set dev/js/enable_js_bundling 0
bin/magento config:set dev/js/minify_files 1
bin/magento config:set dev/js/merge_files 1
bin/magento config:set dev/css/minify_files 1
bin/magento config:set dev/css/merge_css_files 1
bin/magento deploy:mode:set production --skip-compilation

if [ -f insign-ci-disabled-modules.txt ]; then
  while read line; do
    # On ne traite pas les lignes commencçant par un commentaire
    if [ `echo "$line" |grep -E "^[ ]*#"` ]; then continue; fi
    bin/magento module:disable "$line"
  done <insign-ci-disabled-modules.txt
fi
bin/magento deploy:mode:set production
composer dump-autoload -o

# la commande du dessus pourrait être splitée en 2 mais cela demanderait de spécifier les area (--area adminhtml en_US...)
# bin/magento setup:di:compile
# bin/magento setup:static-content:deploy