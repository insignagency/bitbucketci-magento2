#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-magento2
#
if [[ ! "$0" =~ ^/ ]]; then abspath="$(pwd)/$0"; else abspath="$0"; fi
. pipelines/common.sh

echo "127.0.0.1 $test_site_domain" >> /etc/hosts
if [ "$(wget --no-check-certificate -S https://$test_site_domain -o /tmp/headers.txt -O /tmp/page.html; echo $?)" -gt 0 ]; then
  docker-compose -f docker-compose-ci.yml logs apache
  docker-compose -f docker-compose-ci.yml logs php
  docker-compose -f docker-compose-ci.yml exec -T php bash -c "cat /var/log/php*.log"
  docker-compose -f docker-compose-ci.yml ps
  cat /tmp/page.html
fi
track "Affichage des sorties de wget"
cat /tmp/headers.txt
cat /tmp/page.html |grep "block-title"
track "Affichage des éventuelles sorties d'erreurs de magento"
if [ -f var/log/exception.log ]; then cat var/log/exception.log; fi
if [ -d var/report/ ]; then cat var/report/*; fi

if [ "$(grep -iE error /tmp/headers.txt)" ]; then 
  echo "!!!"
  echo "Statut http correspondant à une erreur"; 
  exit 1
fi