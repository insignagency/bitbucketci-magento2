#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-magento2
#
set -e
set -x
cp app/etc/env-bitbucket-services.php app/etc/env.php
cd ./pipelines/deploy/misc/ && ./sync-ci-db.sh $SYNC_CI_DB_FROM && cd ../../../
. pipelines/build/default/composer-version.sh
. pipelines/build/default/install-cms.sh